import React from 'react';
import Button from 'react-bootstrap/Button';
import ReactDOM from 'react-dom';

export class Login extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			resp: null
		}
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleSubmit(event) {
	    event.preventDefault();
	    fetch('http://localhost:3000/login/?username='+event.target.username.value)
		  .then(function(response){ return response.json(); })
		  .then(function(data) {
		  		alert(data["data"])
		  })

	}

	 render() {
	 	return 	(
	 			<div class="container">
		 			<div class="login-form col-md-3 justify-content-md-center">
						    <form onSubmit={this.handleSubmit}>
						        <h2 class="text-center">Log in</h2>       
						        <div class="form-group">
						            <input name="username" type="text" class="form-control" placeholder="Username" required="required" />
						        </div>
						        <div class="form-group">
						            <input type="password" class="form-control" placeholder="Password" required="required" />
						        </div>
						        <div class="form-group">
						            <Button type="submit" >Login</Button>
						        </div>
						        <div class="clearfix">
						            <a href="#" class="pull-right"></a>
						        </div>        
						    </form>
					    <p class="text-center"><a name="data" href="#">Create an Account</a></p>
					</div>
				</div>
				);
	 }
}

